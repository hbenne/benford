
publish:
	rm -r dist || true
	rm -r src/benford_stats.egg-info || true
	python3 setup.py bdist_wheel
	python3 setup.py sdist
	twine upload dist/* 